module bitbucket.org/DmitriyTelepnev/go-api

go 1.15

require (
	github.com/prometheus/client_golang v1.8.0
	github.com/spf13/viper v1.7.1
	github.com/stretchr/testify v1.6.1 // indirect
)
