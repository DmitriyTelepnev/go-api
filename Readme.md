# Шаблон API с метриками, k8s деплойментом, CI-CD

Содержит:
* k8s deployment + metric & healthcheck endpoints
* Implemented HTTP RED metrics over Prometheus metrics
* Dockerfile + configs + env-configs
* Graceful shutdown

Позже добавлю CI-CD

Позволяет только описать логику и хендлеры, по минимуму работая с окружением

## Запуск

```
docker build -t test .
docker run -p 8080:8080 test
```

## Проверка

```
❯ curl http://localhost:8080/ -v
*   Trying ::1...
* TCP_NODELAY set
* Connected to localhost (::1) port 8080 (#0)
> GET / HTTP/1.1
> Host: localhost:8080
> User-Agent: curl/7.64.1
> Accept: */*
> 
< HTTP/1.1 200 OK
< Date: Tue, 24 Nov 2020 04:44:51 GMT
< Content-Length: 0
< 
* Connection #0 to host localhost left intact
* Closing connection 0
```

```
❯ curl http://localhost:8080/health -v
*   Trying ::1...
* TCP_NODELAY set
* Connected to localhost (::1) port 8080 (#0)
> GET /health HTTP/1.1
> Host: localhost:8080
> User-Agent: curl/7.64.1
> Accept: */*
> 
< HTTP/1.1 200 OK
< Date: Tue, 24 Nov 2020 04:45:12 GMT
< Content-Length: 0
< 
* Connection #0 to host localhost left intact
* Closing connection 0
```

```
❯ curl http://localhost:8080/metric
# HELP go_gc_duration_seconds A summary of the pause duration of garbage collection cycles.
# TYPE go_gc_duration_seconds summary
go_gc_duration_seconds{quantile="0"} 0
go_gc_duration_seconds{quantile="0.25"} 0
go_gc_duration_seconds{quantile="0.5"} 0
go_gc_duration_seconds{quantile="0.75"} 0
......
```

## Конфигурация через env

```
docker run -e K8S_METRICENDPOINT="/metrics2" -p 8080:8080 test
```

```
❯ curl http://localhost:8080/metrics2
# HELP go_gc_duration_seconds A summary of the pause duration of garbage collection cycles.
# TYPE go_gc_duration_seconds summary
go_gc_duration_seconds{quantile="0"} 0
go_gc_duration_seconds{quantile="0.25"} 0
go_gc_duration_seconds{quantile="0.5"} 0
go_gc_duration_seconds{quantile="0.75"} 0
......
```