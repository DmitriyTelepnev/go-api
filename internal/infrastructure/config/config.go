package config

import (
	"strings"

	"github.com/spf13/viper"
)

const (
	ApplicationName = "go-api"
	Namespace       = "test-namespace"
)

type (
	Logger struct {
		Level string
	}

	K8S struct {
		Port           uint
		HealthEndpoint string
		MetricEndpoint string
	}

	Root struct {
		Logger Logger
		K8S    K8S
	}
)

func MustConfigure() *Root {
	viper.AutomaticEnv()
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))

	viper.AddConfigPath(".")
	viper.SetConfigName("config")
	viper.SetConfigType("yml")

	readConfErr := viper.ReadInConfig()
	if readConfErr != nil {
		panic(readConfErr)
	}

	cfg := Root{}
	unmarshallErr := viper.Unmarshal(&cfg)
	if unmarshallErr != nil {
		panic(unmarshallErr)
	}

	return &cfg
}
