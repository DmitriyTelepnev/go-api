package main

import (
	"fmt"
	"log"
	"net/http"

	"bitbucket.org/DmitriyTelepnev/go-api/internal/infrastructure/config"
	"bitbucket.org/DmitriyTelepnev/go-api/internal/infrastructure/handler"
	"bitbucket.org/DmitriyTelepnev/go-api/internal/infrastructure/metric"
	"bitbucket.org/DmitriyTelepnev/go-api/internal/infrastructure/signal"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

func main() {
	cfg := config.MustConfigure()
	metrics := metric.NewPrometheus()

	http.Handle(cfg.K8S.MetricEndpoint, promhttp.Handler())
	http.Handle(cfg.K8S.HealthEndpoint, &handler.Health{})
	http.Handle("/", metrics.CollectHTTP(&handler.Main{}))

	go func() {
		listenErr := http.ListenAndServe(fmt.Sprintf(":%d", cfg.K8S.Port), nil)
		if listenErr != nil {
			panic(listenErr)
		}
	}()

	signalHandler := signal.NewHandler(func() error {
		// graceful stop with resource closing
		log.Println("Gracefully stopped")
		return nil
	})

	signalHandler.Poll()
}
